package dk.homestead.joelang.jvmmodel

import com.google.inject.Inject
import dk.homestead.joelang.joeLang.JoeFile
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.xbase.XbaseFactory
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder
import dk.homestead.joelang.joeLang.Actor
import org.eclipse.xtext.xbase.XMemberFeatureCall
import org.eclipse.xtext.xbase.impl.XMemberFeatureCallImpl
import org.eclipse.xtext.xbase.impl.XMemberFeatureCallImplCustom

/**
 * <p>Infers a JVM model from the source model.</p> 
 *
 * <p>The JVM model should contain all elements that would appear in the Java code 
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>     
 */
class JoeLangJvmModelInferrer extends AbstractModelInferrer {

    /**
     * convenience API to build and initialize JVM types and their members.
     */
	@Inject extension JvmTypesBuilder

	/**
	 * The dispatch method {@code infer} is called for each instance of the
	 * given element's type that is contained in a resource.
	 * 
	 * @param element
	 *            the model to create one or more
	 *            {@link JvmDeclaredType declared
	 *            types} from.
	 * @param acceptor
	 *            each created
	 *            {@link JvmDeclaredType type}
	 *            without a container should be passed to the acceptor in order
	 *            get attached to the current resource. The acceptor's
	 *            {@link IJvmDeclaredTypeAcceptor#accept(org.eclipse.xtext.common.types.JvmDeclaredType)
	 *            accept(..)} method takes the constructed empty type for the
	 *            pre-indexing phase. This one is further initialized in the
	 *            indexing phase using the closure you pass to the returned
	 *            {@link IPostIndexingInitializing#initializeLater(org.eclipse.xtext.xbase.lib.Procedures.Procedure1)
	 *            initializeLater(..)}.
	 * @param isPreIndexingPhase
	 *            whether the method is called in a pre-indexing phase, i.e.
	 *            when the global index is not yet fully updated. You must not
	 *            rely on linking using the index if isPreIndexingPhase is
	 *            <code>true</code>.
	 */
	def dispatch void infer(Actor actor, IJvmDeclaredTypeAcceptor acceptor, boolean isPreIndexingPhase) {
		acceptor.accept(actor.toClass("dk.homestead.joelang." + actor.name)) [
			p("Generating class " + actor.name);
			// System.out.println(actor.fields.size + " fields.");
			for (f : actor.fields) {
				// System.out.println("Attempting to add field for " + actor.name + ":" + f.name);
				it.members += toField(f, f.name, f.jvmType);
			}
			// p(actor.protocols.size + " protocols.");
			for (p : actor.protocols) {
				// p((if (p.isNegatedProt) "^" else "") + ": at most " + p.protocol.steps.size + " inputs.");
				for (step : p.protocol.steps) {
					if (step.asInput && !p.isNegatedProt || step.asOutput && p.isNegatedProt) {
						// Find matching ProtocolStep implementation.
						for (method : actor.methods.filter[name == step.name]) {
							// p("Generating method for " + actor.name + ":" + step.name);
							it.members += toMethod(p, step.name, null) [
								for (param : method.parameters) {
									parameters += toParameter(param, param.name, param.jvmType);
								}
								// Add implicit "sender" var to Expression.
								val _sender = XbaseFactory.eINSTANCE.createXVariableDeclaration => [
									writeable = false;
									name = "sender";
								]
								method.body.eResource.contents += _sender;
								body=method.body;
							]
						}
					}
				}
			}
		]	
	}
	
	def p(String s) {
		System.out.println(s);
	}
}

